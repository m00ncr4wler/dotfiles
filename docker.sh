#!/usr/bin/env bash

# https://www.robinwieruch.de/docker-macos

# Install packages
PACKAGES=(
    docker
    docker-machine
    docker-compose
)
brew install ${PACKAGES[@]}

APPS=(
    virtualbox
)
brew install ${APPS[@]}

docker-machine create --driver virtualbox default
docker-machine start default

# add to profile
# eval $(docker-machine env default)
