#!/usr/bin/env bash

###
# Add to bashrc
# export PATH="/usr/local/opt/bzip2/bin:$PATH"
# export PATH="/usr/local/opt/libiconv/bin:$PATH"
# 
# [[ -e ~/.phpbrew/bashrc ]] && source ~/.phpbrew/bashrc
# phpbrew use 7.4.21

export PHP_VERSION=8.1.11

brew install phpbrew php@7.4
brew link --overwrite php@7.4

phpbrew init
phpbrew lookup-prefix homebrew

BUILD=(
    pkg-config
    bzip2
    libpng
    gmp
    libiconv
    libzip
    autoconf
    krb5
    zlib
    curl
    imap-uw
    libsodium
    imagemagick
)
brew install ${BUILD[@]}

brew tap lpinca/libmpdec
brew install libmpdec

#export LDFLAGS="${LDFLAGS} -L/usr/local/opt/icu4c/lib"
#export CPPFLAGS="${CPPFLAGS} -I/usr/local/opt/icu4c/include"
#export PKG_CONFIG_PATH=/usr/local/opt/icu4c/lib/pkgconfig:"$PKG_CONFIG_PATH"

phpbrew known --update

phpbrew install $PHP_VERSION +bcmath +bz2 +calendar +ctype +curl +dom +exif +fileinfo +filter +gettext +gmp +hash +iconv +imap +intl +json +mbstring +mysql +openssl +pcre +pdo +phar +readline +session +soap +sockets +tokenizer +wddx +xml +zip +zlib +sodium +gd -- --with-gd=shared

source ~/.phpbrew/bashrc
phpbrew use php-$PHP_VERSION

phpbrew ext install xdebug
phpbrew ext install redis
phpbrew ext install decimal
phpbrew ext install apcu
phpbrew ext install pcntl
phpbrew ext install opcache
phpbrew ext install imagick
