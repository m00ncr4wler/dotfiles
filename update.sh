#!/usr/bin/env bash

# update apps
APPS=(
    slack
    zoom
    gpg-suite
    vagrant
    virtualbox
    phpstorm
    sequel-ace
    appcleaner
    visual-studio-code
    1password
    bitwarden
    postman
    proxyman
    spotify
    osxfuse
)
brew reinstall --cask ${APPS[@]}
