#!/usr/bin/env bash

# Install brew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

# Install packages
PACKAGES=(
    coreutils
    gnu-sed
    gnu-tar
    gnu-indent
    gnu-which
    grep
    findutils
    bash
    git
    jq
    npm
    composer
    tmux
    tree
    vim
    wget
    htop
    ansible
    1password-cli
    bitwarden-cli
    lynis # mac security
    chkrootkit # rootkit checker
    swiftgen #ios
    swiftlint #ios
)
brew install ${PACKAGES[@]}

# cocoapads
sudo gem install cocoapods
# cd ~/Repositories/bank-ios/ && pods install

# Install fonts
brew tap homebrew/cask-fonts 
FONTS=(
    font-meslo-for-powerline
)
brew install --cask ${FONTS[@]}

brew install romkatv/powerlevel10k/powerlevel10k
echo 'source /usr/local/opt/powerlevel10k/powerlevel10k.zsh-theme' >> ~/.zshrc


# Install apps
brew install homebrew/cask
APPS=(
    slack
    zoom
    gpg-suite
    vagrant
    virtualbox
    phpstorm
    sequel-ace
    appcleaner
    visual-studio-code
    1password
    bitwarden
    postman
    proxyman
    spotify
    osxfuse
)
brew install --cask ${APPS[@]}

MODULES=(
    redoc-cli@0.9.11
)
npm install ${MODULES[@]} -g
