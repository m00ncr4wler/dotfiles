# Run


PHP 7.4.16 install via brew & phpbrew
```bash
/bin/bash -c "$(curl -fsSL https://gitlab.com/m00ncr4wler/dotfiles/-/raw/master/php.sh)"
```

## Add to bashrc
```bash
export PATH="/usr/local/opt/bzip2/bin:$PATH"
export PATH="/usr/local/opt/libiconv/bin:$PATH"
 
[[ -e ~/.phpbrew/bashrc ]] && source ~/.phpbrew/bashrc
phpbrew use php-7.4.16
```

# Clean up
```bash
phpbrew clean php-7.4.16
```

# Update cask
```bash
/bin/bash -c "$(curl -fsSL https://gitlab.com/m00ncr4wler/dotfiles/-/raw/master/update.sh)"
```
# vim
```bash
# apt install vim-gtk
echo "$(curl -fsSL https://gitlab.com/m00ncr4wler/dotfiles/-/raw/master/.vimrc)" > ~/.vimrc
```

# Cake
```bash
/bin/bash -c "$(curl -fsSL https://gitlab.com/m00ncr4wler/dotfiles/-/raw/master/cake.sh)"
```
